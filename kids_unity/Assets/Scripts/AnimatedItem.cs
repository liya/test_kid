﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimatedItem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Animate();
    }
    public abstract void Animate();
}
