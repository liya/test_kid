﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickPicture : MonoBehaviour
{
    public AnimatedItem interactiveObject;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickToAppear();
        }
    }

    public void ClickToAppear() {
        interactiveObject.gameObject.SetActive(true);
        interactiveObject.Animate();
    }
}
