﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFromAToB : AnimatedItem
{
    //you can give a list of points for the movement trajectory
    public Vector3[] positions;
    public float duration = 4;

    private float lerpSpeed;
    private float lerpAmount = 0;
    private int i = 0;
    private int numberOfPoints;

    public override void Animate()
    {
        //The lerpfloat should always be reset back to 0 otherwise you'd start teleporting.
        if (lerpAmount >= 1f)
        {
            lerpAmount = 0;
        }
        lerpAmount += lerpSpeed * Time.deltaTime;
        //Here the lerp from place to place takes place, I used %3 because I've 3 elements in my array, this way it keeps looping between these positions.
        transform.position = Vector3.Lerp(positions[i % numberOfPoints], positions[(i + 1) % numberOfPoints], lerpAmount);

        //since lerp never actually reaches its target you need to check for the distance and can't just say if the lerps final position is reached up "i" by 1.
        if (Vector3.Distance(transform.position, positions[(i + 1) % numberOfPoints]) < 0.05)
        {
            i++;
            lerpAmount = 1;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
        numberOfPoints = positions.Length;
        lerpSpeed = numberOfPoints / duration;
    }

}
